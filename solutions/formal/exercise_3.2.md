```
board |-> "fork"*"fork"*"lock" |-
  board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); put("lock")
‖ board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); put("lock")

⇓

board |-> "fork"*"fork" |-
  board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); put("lock")
‖ board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); put("lock")

⇓

board |-> "fork" |-
  board.get("fork"); board.put("fork"); board.put("fork").put("lock")
‖ board.get("lock"); board.get("fork")' board.get("fork"); board.put("fork"); board.put("fork"); put("lock")

⇓

board |-> nil |-
  board.put("fork"); board.put("fork"); put("lock")
‖ board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork").put("lock")

⇓

board |-> "fork" |-
  board.put("fork"); put("lock")
‖ board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); put("lock")

⇓

board |-> "fork"*"fork" |-
  put("lock")
‖ board.get("lock").board.get("fork").board.get("fork").board.put("fork").board.put("fork").put("lock")

⇓

board |-> "fork"*"fork"*"lock" |-
  board.get("lock"); board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); board.put("lock")

⇓

board |-> "fork"*"fork" |-
  board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); board.put("lock")

⇓

board |-> "fork" |-
  board.get("fork"); board.put("fork"); board.put("fork"); board.put("lock")

⇓

board |-> nil |-
  board.put("fork"); board.put("fork"); board.put("lock")

⇓

board |-> "fork" |-
  board.put("fork"); board.put("lock")

⇓

board |-> "fork"*"fork" |-
  board.put("lock")

⇓

board |-> "fork"*"fork"*"lock" |-
  0


```
