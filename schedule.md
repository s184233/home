## Distributed programming with tuple spaces

This first group of three lectures provide the basic knowledge to get started programming distributed applications with tuple spaces. They introduce a *process-oriented* approach to modelling and programming pSpace applications where applications are conceived as a set of processes (possibly running in different applications and hosts) that interact through tuple spaces.

### 1. Programming with spaces
 * Preparation: get familiar with [pSpaces](https://github.com/pSpaces/), read [tutorial 01](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md) and play with [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces).
 * Content: [tutorial 01]( https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md), slides and video of recorded lecture.
 * Exercises: Exercises 1.1-1.5 from the [exercise page](exercises.md).

### 2. Concurrent programming
 * Preparation: read [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md), play with [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces), and have a look at document `Program Graph Semantics of Tuple Spaces.pdf`.
 * Content: [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md), slides and video of recorded lecture.
 * Exercises: Exercises: 2.1-2.6 from the [exercise page](exercises.md).

### 3. Distributed programming
 * Preparation: read [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md) and this [short note on distributed programming patterns](https://gitlab.gbar.dtu.dk/02148/home/blob/master/distributed.md).
 * Content: slides, video of recorded lecture, [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md) and this [short note on distributed programming patterns](https://gitlab.gbar.dtu.dk/02148/home/blob/master/distributed.md).
 * Exercises: Exercises 3.1-3.3 in [exercise page](exercises.md).

# Coordination models

So far we have adopted a *process-oriented* where the focus is on programming processes that, when running interact with each other.  The process-oriented approach is a *bottom-up* approach to programming distributed applications: the application logic emerges from the composition of the processes. Distributed applications can also be modelled using *top-down* approaches where the global coordination logic is defined first. The local coordination logic of the processes is then synthetised from that global logic. One advantage of *top-down* approaches is that, under certain conditions, the obtained application enjoys good properties (e.g. deadlock-freedom) by design, so that less verifications are needed.

The next three chapters present a set of top-down approaches, explaining how to adopt them in pSpaces. In particular, we will consider:
 * *interaction-oriented* programming (protocols) where we focus on the interactions between a set of agents.
 * *task-oriented* programming (workflows) where we focus on tasks to be completed and the order among them.
 * *stream-oriented* programming (dataflows) where we focus on streams of data to be processesed by a network of data processing and routing units.

### 4. Interaction-oriented programming (protocols)
 * Preparation: read these [notes on interaction-oriented programming](protocols.md), and have a look at the slides.
 * Content: [notes on interaction-oriented programming](protocols.md), slides and video of recorded lecture.
 * Exercises: Exercise 4.1 in [exercise page](exercises.md).

### 5. Task-oriented programming (workflows)
 * Preparation: read these [notes on task-oriented programming](workflows.md), and have a look at the slides.
 * Content: [notes on task-oriented programming](https://gitlab.gbar.dtu.dk/02148/home/blob/master/workflows.md), slides and video of recorded lecture.
 * Exercises: Exercises 5.1-5.3 in [exercise page](exercises.md).

### 6. Stream-oriented programming (dataflows)
 * Preparation: read these [notes on stream-oriented programming](streams.md).
 * Content: [notes on stream-oriented programming](https://gitlab.gbar.dtu.dk/02148/home/blob/master/streams.md), slides and video of recorded lecture.
 * Exercises: Exercise 6.1 in [exercise page](exercises.md).

<!---

# Extra lectures

### A. Semantics of pSpace programs
 * Preparation: play with [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces), and read the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md) page. If you had 02141 you can also have a look at file `Program Graph Semantics of Tuple Spaces.pdf` in FileSharing.
 * Content:  [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces), the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Exercises: Exercises 4.1-4.3 in [exercise page](exercises.md).
 * Related courses at DTU: Computer Science Modelling [02141](http://kurser.dtu.dk/course/02141).

### B. Modelling and analysing pSpaces programs with Spin
 * Preparation: read about [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Content: brief Spin tutorial and notes on [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Exercises: Exercise 5.1-5.3 in [exercise page](exercises.md).
 * Related courses at DTU: Model Checking [02246](http://kurser.dtu.dk/course/02246).

### C. Secure Spaces
 * Preparation: read these [notes on secure tuple spaces](secure-spaces.md).
 * Content: [notes on secure tuple spaces](secure-spaces.md).
 * Exercises: Exercise 9.1 in [exercise page](exercises.md).
 * Related courses at DTU: Data Security [02239](http://kurser.dtu.dk/course/02239), Language-based Security [02244](http://kurser.dtu.dk/course/02244), Network Security [02233](http://kurser.dtu.dk/course/02233).

 --->
